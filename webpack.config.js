var webpack = require('webpack');
var webpackMerge = require('webpack-merge');
var path = require('path');
var WebpackNotifierPlugin = require('webpack-notifier');
var NpmInstallPlugin = require('npm-install-webpack-plugin');
var autoprefixer = require('autoprefixer');

const TARGET = process.env.npm_lifecycle_event;
console.log("target event is " + TARGET);

var outputFileName = "app";
outputFileName += (TARGET === 'prod' ? ".min.js" : ".js");

var common = {
  entry: './src/script/index.jsx',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: outputFileName
  },
  module: {
    rules: [
      {
        test: /\.js[x]?$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader?presets[]=es2015&presets[]=react&presets[]=stage-2'
        }
      },
      {
        test: /\.scss$/,
        loaders: ['style-loader', 'css-loader', {
        loader: 'postcss-loader', // postcss loader so we can use autoprefixer
        options: {
            config: {
              path: 'postcss.config.js'
            }
          }
        },'sass-loader']
      },
      {
        test: /\.less$/,
        loaders: ['style-loader', 'css-loader', 'less-loader']
      },
      {
        test: /\.css$/,
        use: [ 'style-loader', 'css-loader' ]
      },
      {
        test: /\.woff$/,
        loader: "url-loader?limit=10000&mimetype=application/font-woff&name=[path][name].[ext]"
      },
      {
        test: /\.woff2$/,
        loader: "url-loader?limit=10000&mimetype=application/font-woff2&name=[path][name].[ext]"
      },
      {
        test: /\.(eot|ttf|svg|gif|png|jpg|otf)$/,
        loader: "url-loader"
      }
    ]
  },
  plugins: [
    new webpack.ProvidePlugin({
      jQuery: 'jquery',
      $: 'jquery',
      jquery: 'jquery',
      "window.jQuery": 'jquery'
    }),
    new webpack.LoaderOptionsPlugin({
      options: {
        postcss: [
          autoprefixer({
               browsers: ['last 3 versions']
          })
        ]
      }
    }),
    new WebpackNotifierPlugin()
  ]
};

if (TARGET === 'dev' || !TARGET) {
  module.exports = webpackMerge(common, {
    devtool: 'source-map',
    plugins: [
      new NpmInstallPlugin({
        save: true // --save
      })
    ]
  });
}
