import React from 'react';
import ReactDOM from 'react-dom';
import thunk from 'redux-thunk';
import { Route, HashRouter } from 'react-router-dom';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import rootReducer from './reducers';
import { syncHistoryWithStore, routerReducer } from 'react-router-redux';
import { createBrowserHistory } from 'history';

import bootstrap from 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.css';

import '../style/main.scss';

import Repairs from './views/repairs/repairs.jsx';
import Users from './views/users/users.jsx';
import Access from './views/access/AccessContainer.jsx';
import AccessStore from './views/access/AccessStore.js';

// Add the reducer to your store on the `routing` key
const store = createStore(
  combineReducers({
    rootReducer,
    routing: routerReducer,
  }),
  applyMiddleware(thunk),
);

// Create an enhanced history that syncs navigation events with the store
const history = syncHistoryWithStore(createBrowserHistory(), store);

const homeRoute = (
  <Route
    exact
    path="/"
    render={() =>
      (AccessStore.getId() ? <Route component={Repairs} /> : <Route component={Access} />)}
  />
);

ReactDOM.render(
  <Provider store={store}>
    <HashRouter>
      <div>
        {homeRoute}
        <Route path="/lostpassword" component={Access} type="lostpassword" />
        <Route path="/register" component={Access} type="register" />
        <Route path="/login" component={Access} type="login" />
        <Route
          exact
          path="/repairs"
          render={() =>
            (AccessStore.getId() ? <Route component={Repairs} /> : <Route component={Access} />)}
        />
        <Route
          exact
          path="/users"
          render={() =>
            (AccessStore.getId() && AccessStore.isModerator() ? (
              <Route component={Users} />
            ) : (
              homeRoute
            ))}
        />
      </div>
    </HashRouter>
  </Provider>,
  document.getElementById('content'),
);
