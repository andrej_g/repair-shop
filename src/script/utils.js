import moment from 'moment';
import { DATE_FORMAT, TIME_FORMAT } from './globalConstants';

/**
 * Defines descending order by date
 *
 * @param a - object with date property
 * @param b - object with date property
 * @returns {number} - 1 if a is greater than b by the ordering criterion, 0 if they are equal, otherwise -1
 */
export function sortByDateDesc(a, b) {
  if (
    moment(`${a.date} ${a.time}`, `${DATE_FORMAT} ${TIME_FORMAT}`).isAfter(
      moment(`${b.date} ${b.time}`, `${DATE_FORMAT} ${TIME_FORMAT}`),
    )
  ) { return -1; }
  if (
    !moment(`${a.date} ${a.time}`, `${DATE_FORMAT} ${TIME_FORMAT}`).isAfter(
      moment(`${b.date} ${b.time}`, `${DATE_FORMAT} ${TIME_FORMAT}`),
    )
  ) { return 1; }
  return 0;
}
