import { combineReducers } from 'redux';
import { repairs, repairsHasErrored } from './views/repairs/reducers';
import { users, usersHasErrored } from './views/users/reducers';

export default combineReducers({
  repairs,
  repairsHasErrored,
  users,
  usersHasErrored,
});
