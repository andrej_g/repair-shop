import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './header.scss';
import {NavLink, Link} from 'react-router-dom';

import AccessStore from '../views/access/AccessStore.js';

export default class Header extends Component {

    constructor(props) {
        super(props);
        this._handleLogoutClick = this._handleLogoutClick.bind(this);
    }

    _handleLogoutClick() {
        AccessStore.resetStore();
    }

    render() {

        let usersLink = null;

        if (AccessStore.isModerator()) {
            usersLink = <li>
              <NavLink to="/users" activeClassName="active">Users</NavLink>
            </li>
        }

        return (
            <nav className="navbar navbar-default">
              <div className="container-fluid">

                <div className="navbar-header">
                  <button type="button" className="navbar-toggle collapsed" data-toggle="collapse"
                  data-target="#main-navbar-collapse" aria-expanded="false">
                    <span className="sr-only">Toggle navigation</span>
                    <span className="icon-bar"></span>
                    <span className="icon-bar"></span>
                    <span className="icon-bar"></span>
                  </button>
                  <Link to="/" className="navbar-brand">Repair Shop</Link>
                </div>

                <div className="collapse navbar-collapse" id="main-navbar-collapse">
                  <ul className="nav navbar-nav">
                    <li>
                      <NavLink to="/repairs" activeClassName="active">Repairs</NavLink>
                    </li>
                    {usersLink}
                  </ul>
                  <ul className="nav navbar-nav navbar-right">
                    <li onClick={this._handleLogoutClick}>
                      <Link className="link-logout" to="/login">Logout</Link>
                    </li>
                  </ul>
                </div>
              </div>
            </nav>
        );
    }
}
