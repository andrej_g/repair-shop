import firebase from 'firebase';

export const FIREBASE_CONFIG = {
  apiKey: 'AIzaSyCn5fRW63lNiKYEYwgcjoufsDPRoxaZZBE',
  authDomain: 'repairs-shop-a1e3b.firebaseapp.com',
  databaseURL: 'https://repairs-shop-a1e3b.firebaseio.com',
  projectId: 'repairs-shop-a1e3b',
  storageBucket: 'repairs-shop-a1e3b.appspot.com',
  messagingSenderId: '1027267551027',
};

export const FIREBASE_API_REF = firebase.initializeApp(FIREBASE_CONFIG);
export const DATE_FORMAT = 'DD/MM/YYYY';
export const TIME_FORMAT = 'HH:mm';
