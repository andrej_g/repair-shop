import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import './style/accessContainer.scss';
import { notify } from 'bootstrap-notify';
import AccessActions from './AccessActions';

/**
 * Component represents 3 views - Login, Register and Lost Password
 */
export default class AccessContainer extends Component {
  constructor(props) {
    super(props);

    this.state = { errorMessage: '' };

    this.togglePassword = this.togglePassword.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  togglePassword() {
    $(ReactDOM.findDOMNode(this.refs.showPw)).toggleClass('active');
    const formPassword = ReactDOM.findDOMNode(this.refs.formPassword);
    formPassword.type = formPassword.type === 'password' ? 'text' : 'password';
  }

  handleSubmit(e) {
    const that = this;
    e.preventDefault();

    if (that.props.location.pathname === '/' || that.props.location.pathname.includes('login')) {
      AccessActions.login(
        document.getElementById('formEmail').value,
        document.getElementById('formPassword').value,
      )
        .then((path) => {
          that.context.router.history.push(path);
        })
        .catch((error) => {
          that.setState({ errorMessage: error.message });
        });
    } else if (that.props.location.pathname.includes('register')) {
      if (that.state.errorMessage.length > 0) {
        that.setState({ errorMessage: '' });
      }

      const notify = $.notify('<strong>Saving</strong> new user...', {
        animate: {
          enter: 'animated fadeInRight',
          exit: 'animated fadeOutRight',
        },
        allow_dismiss: false,
        showProgressbar: true,
        delay: 3000,
      });

      AccessActions.register(
        document.getElementById('formEmail').value,
        document.getElementById('formPassword').value,
      )
        .then((path) => {
          $('[data-notify="progressbar"]').hide();
          notify.update({
            animate: {
              enter: 'animated fadeInRight',
              exit: 'animated fadeOutRight',
            },
            type: 'success',
            delay: 2000,
            message: '<strong>Success</strong> New user has been created!',
          });

          setTimeout(() => {
            $.notifyClose('top-right');
            that.context.router.history.push(path);
          }, 2000);
        })
        .catch((error) => {

          $('[data-notify="progressbar"]').hide();
          notify.update({
            animate: {
              enter: 'animated fadeInRight',
              exit: 'animated fadeOutRight',
            },
            type: 'danger',
            delay: 2000,
            message: `<strong>Error</strong> ${error.message}`,
          });

          setTimeout(() => {
            $.notifyClose('top-right');
          }, 2000);
        });
    } else if (that.props.location.pathname.includes('lostpassword')) {
      AccessActions.resetPassword(document.getElementById('formEmail').value)
        .then((path) => {
          const notify = $.notify('Check your email to change your password.', {
            animate: {
              enter: 'animated fadeInRight',
              exit: 'animated fadeOutRight',
            },
            allow_dismiss: true,
            showProgressbar: false,
            type: 'success',
            delay: 2000,
          });
          setTimeout(() => {
            $.notifyClose('top-right');
            that.context.router.push(path);
          }, 2000);
        })
        .catch((error) => {
          that.setState({ errorMessage: error.message });
        });
    }

    return false;
  }

  render() {
    let errorMessage = null;

    if (this.state.errorMessage !== '') {
      errorMessage = (
        <div className="row error server_error_msg">
          <p>{this.state.errorMessage}</p>
        </div>
      );
    }

    let submitButtonLabel = null;
    let registerLink = null;
    let header = null;

    if (this.props.location.pathname === '/' || this.props.location.pathname.includes('login')) {
      submitButtonLabel = 'Login';
      registerLink = (
        <span>
          <Link to="/register">Registration</Link>
        </span>
      );
      header = <h1>Login</h1>;
    } else if (this.props.location.pathname.includes('register')) {
      submitButtonLabel = 'Register';
      registerLink = (
        <span>
          <Link to="/login">Login</Link>
        </span>
      );
      header = <h1>Registration</h1>;
    } else if (this.props.location.pathname.includes('lostpassword')) {
      submitButtonLabel = 'Reset password';
      registerLink = (
        <span>
          <Link to="/login">Login</Link>
        </span>
      );
      header = <h1>Lost password</h1>;
    }

    const loginField = (
      <div className="row">
        <div className="three columns vertical-align">
          <label htmlFor="formEmail">Email</label>
        </div>
        <div className="nine columns">
          <input name="formEmail" type="email" id="formEmail" required />
        </div>
      </div>
    );

    let passwordField = null;

    if (!this.props.location.pathname.includes('lostpassword')) {
      passwordField = (
        <div className="row">
          <div className="three columns vertical-align">
            <label htmlFor="formPassword">Password</label>
          </div>
          <div className="nine columns">
            <span className="showPw" ref="showPw" onClick={this.togglePassword}>
              <i className="fa fa-eye" aria-hidden="true" />
            </span>
            <input
              name="formPassword"
              type="password"
              ref="formPassword"
              id="formPassword"
              required
            />
          </div>
        </div>
      );
    }

    const lostPasswordLink = <Link to="/lostpassword">Forgot your passport?</Link>;

    return (
      <div className="login-wrapper">
        {/* [if lt IE 8]>
          <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif] */}
        <div className="block header-bar">
          <div className="wrapper">
            <div className="container">
              <div className="logo">
                <span>
                  <Link to="/">Repair shop</Link>
                </span>
              </div>
              <div className="switch dropdown">{registerLink}</div>
            </div>
          </div>
        </div>

        <div className="block grey header">
          <div className="wrapper">
            <div className="container">
              {header}
              <h2>Let's manage repairs</h2>
            </div>
          </div>
        </div>

        <div className="block white">
          <div className="wrapper">
            <div className="container">
              <form onSubmit={this.handleSubmit}>
                <div className="form-wrapper">
                  {errorMessage}
                  <div className="grid">
                    {loginField}
                    {passwordField}
                    <div className="row">
                      <div className="three columns">&nbsp;</div>
                      <div className="nine columns">
                        <button id="register" className="button green invert" type="submit">
                          {submitButtonLabel}
                          <i className="fa fa-long-arrow-right" aria-hidden="true" />
                        </button>
                      </div>
                    </div>
                    <div className="row supp_fallback">
                      <div className="three columns vertical-align">&nbsp;</div>
                      <div className="nine columns">
                        {lostPasswordLink} Need help?&nbsp;
                        <a href="mailto:support@repair-shop.com?Subject=Need%20help">
                          Let us know!
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>

        {/* Footer */}
        <div id="footer" className="block grey">
          <div className="wrapper">
            <div className="container">
              <ul className="social">
                <li className="web">
                  <i className="fa fa-home" aria-hidden="true" />
                  <a href="https://www.repairshop.com" target="_blank" title="Website">
                    Website
                  </a>
                </li>
                <li className="fb">
                  <i className="fa fa-facebook-square" aria-hidden="true" />
                  <a href="https://facebook.com" target="_blank" title="Facebook">
                    Facebook
                  </a>
                </li>
                <li className="tw">
                  <i className="fa fa-twitter-square" aria-hidden="true" />
                  <a href="https://twitter.com" target="_blank" title="Twitter">
                    Twitter
                  </a>
                </li>
                <li className="yt">
                  <i className="fa fa-youtube-play" aria-hidden="true" />
                  <a href="https://www.youtube.com" target="_blank" title="YouTube">
                    YouTube
                  </a>
                </li>
                <li className="ln">
                  <i className="fa fa-linkedin-square" aria-hidden="true" />
                  <a href="https://www.linkedin.com" target="_blank" title="LinkedIn">
                    LinkedIn
                  </a>
                </li>
              </ul>
              <div className="copyright">&copy; Repair shop 2017</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

AccessContainer.contextTypes = {
  router: PropTypes.shape({
    history: PropTypes.object.isRequired,
  }),
};
