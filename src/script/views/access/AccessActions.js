import axios from 'axios';
import AccessStore from './AccessStore';
import Promise from 'bluebird';
import { FIREBASE_API_REF, FIREBASE_CONFIG } from '../../globalConstants';

const AccessActions = {
  /**
     * Login user in firebase
     *
     * @param email - email login
     * @param password - password login
     */
  login(email, password) {
    return new Promise(((resolve, reject) => FIREBASE_API_REF.auth()
      .signInWithEmailAndPassword(email, password)
      .then(authData => authData.getIdToken(false).then(
        (accessToken) => {
          AccessStore.setCredentials(authData.uid, accessToken);
          return FIREBASE_API_REF.database()
            .ref(`users/${authData.uid}`)
            .once('value')
            .then((snapshot) => {
              if (!snapshot.val().active) {
                return reject(new Error("User doesn't exist"));
              }
              if (snapshot.hasChild('mod') && snapshot.val().mod) {
                AccessStore.setModeratorRole(true);
              }
              return resolve('/repairs');
            });
        },
        error => reject(error),
      ))));
  },

  /**
     * Register new user in firebase and add new user to database
     *
     * @param email - user email
     * @param password - user password
     */
  register(email, password) {
    return new Promise(((resolve, reject) => FIREBASE_API_REF.auth()
      .createUserWithEmailAndPassword(email, password)
      .then(userData => FIREBASE_API_REF.auth()
        .signInWithEmailAndPassword(email, password)
        .then(userData => userData
          .getIdToken(false)
          .then(
            (accessToken) => {
              AccessStore.setCredentials(userData.uid, accessToken);
            },
            error => reject(error),
          )
          .then(() => axios({
            method: 'put',
            url: `${FIREBASE_CONFIG.databaseURL}/users/${userData.uid}.json`,
            data: JSON.stringify({
              email,
              active: true,
              mod: false,
            }),
            params: {
              auth: AccessStore.getToken(),
            },
          }))
          .then(() => resolve('/repairs'))
          .catch((error) => {
            reject(error);
          }))
        .catch((error) => {
          reject(error);
        }))));
  },

  /**
     * Sends a password reset email to the given email address
     *
     * @param email to reset
     */
  resetPassword(email) {
    return new Promise(((resolve, reject) => FIREBASE_API_REF.auth()
      .sendPasswordResetEmail(email)
      .then(
        () => resolve('/login'),
        error => reject(error),
      )));
  },
};

export default AccessActions;
