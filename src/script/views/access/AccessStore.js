import Cookies from 'cookies-js';
import { FIREBASE_API_REF } from '../../globalConstants';

const accessStoreInitObj = {
  id: null,
  token: null,
  isMod: null,
};

/**
 * Contains user id, token and user role
 */
const AccessStore = Object.assign(accessStoreInitObj, {
  isModerator() {
    if (this.isMod === null && typeof Cookies.get('repair_shop_user_isMod') !== 'undefined') {
      return Cookies.get('repair_shop_user_isMod');
    }
    return this.isMod;
  },

  getId() {
    if (!this.id && typeof Cookies.get('repair_shop_user_id') !== 'undefined') {
      return Cookies.get('repair_shop_user_id');
    }
    return this.id;
  },

  getToken() {
    if (!this.token && typeof Cookies.get('repair_shop_user_token') !== 'undefined') {
      return Cookies.get('repair_shop_user_token');
    }
    return this.token;
  },

  setCredentials(newId, newToken) {
    this.id = newId;
    this.token = newToken;
    const inOneHour = new Date(new Date().getTime() + (60 * 60 * 1000));
    Cookies.set('repair_shop_user_id', newId, { expires: inOneHour });
    Cookies.set('repair_shop_user_token', newToken, { expires: inOneHour });
  },

  setModeratorRole(isMod) {
    this.isMod = isMod;
    const inOneHour = new Date(new Date().getTime() + (60 * 60 * 1000));
    Cookies.set('repair_shop_user_isMod', isMod, { expires: inOneHour });
  },

  resetStore() {
    FIREBASE_API_REF.auth().signOut();
    this.id = null;
    this.token = null;
    this.isMod = null;
    Cookies.set('repair_shop_user_id', '');
    Cookies.set('repair_shop_user_token', '');
    Cookies.set('repair_shop_user_isMod', '');
  },
});

export default AccessStore;
