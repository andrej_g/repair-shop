import Immutable from 'immutable';

export function usersHasErrored(state = {}, action) {
  switch (action.type) {
    case 'USERS_HAS_ERRORED':
      return {
        errorMessage: action.errorMessage,
      };
    default:
      return state;
  }
}

export function users(state = {}, action) {
  switch (action.type) {
    case 'USERS_FETCH_DATA_SUCCESS': {
      const tempUsers = [];
      if (action.items) {
        Object.keys(action.items).forEach((userId) => {
          if (Object.prototype.hasOwnProperty.call(action.items, userId)) {
            const currItem = action.items[userId];
            currItem.id = userId;
            delete currItem.active;
            tempUsers.push(currItem);
          }
        });
      }
      return {
        ...state,
        userList: Immutable.List(tempUsers),
      };
    }
    case 'USER_ADDED_SUCCESS': {
      let currUserList = state.userList;
      currUserList = currUserList.push(action.userObj);
      return {
        ...state,
        userList: currUserList,
        successMessage: action.successMessage,
      };
    }
    case 'USER_DELETED_SUCCESS': {
      let currUserList = state.userList;
      for (let i = 0; i < currUserList.size; i += 1) {
        if (currUserList.get(i).id.toString() === action.userObj.id) {
          currUserList = currUserList.delete(i);
          break;
        }
      }
      return {
        ...state,
        userList: currUserList,
        successMessage: action.successMessage,
      };
    }
    case 'USER_UPDATE_EMAIL_SUCCESS': {
      let currUserList = state.userList;
      for (let i = 0; i < currUserList.size; i += 1) {
        if (currUserList.get(i).id === action.userObj.id) {
          const currUserObj = action.userObj;
          currUserObj.id = currUserObj.newId;
          currUserList = users.update(i, () => currUserObj);
          break;
        }
      }
      return {
        ...state,
        userList: currUserList,
        successMessage: action.successMessage,
      };
    }
    case 'USER_UPDATE_ROLE_SUCCESS': {
      let currUserList = state.userList;
      for (let i = 0; i < currUserList.size; i += 1) {
        if (currUserList.get(i).id === action.userObj.id) {
          currUserList = currUserList.update(i, () => action.userObj);
          break;
        }
      }
      return {
        ...state,
        userList: currUserList,
        successMessage: action.successMessage,
      };
    }
    case 'USER_PASSRESET_SUCCESS': {
      return {
        ...state,
        successMessage: action.successMessage,
      };
    }
    default:
      return state;
  }
}
