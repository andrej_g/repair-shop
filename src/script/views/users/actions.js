import axios from 'axios';
import { FIREBASE_CONFIG, FIREBASE_API_REF } from '../../globalConstants';
import AccessStore from '../access/AccessStore';

export function usersHasErrored(errorMessage) {
  return {
    type: 'USERS_HAS_ERRORED',
    errorMessage,
  };
}

export function usersFetchDataSuccess(items) {
  return {
    type: 'USERS_FETCH_DATA_SUCCESS',
    items,
  };
}

export function usersAddedSuccess(userObj, successMessage) {
  return {
    type: 'USER_ADDED_SUCCESS',
    userObj,
    successMessage,
  };
}

export function userDeletedSuccess(userObj, successMessage) {
  return {
    type: 'USER_DELETED_SUCCESS',
    userObj,
    successMessage,
  };
}

export function userUpdateEmailSuccess(userObj, successMessage) {
  return {
    type: 'USER_UPDATE_EMAIL_SUCCESS',
    userObj,
    successMessage,
  };
}

export function userUpdateRoleSuccess(userObj, successMessage) {
  return {
    type: 'USER_UPDATE_ROLE_SUCCESS',
    userObj,
    successMessage,
  };
}

export function userPassResetSuccess(successMessage) {
  return {
    type: 'USER_PASSRESET_SUCCESS',
    successMessage,
  };
}

export function getAllUsers() {
  return (dispatch) => {
    axios({
      method: 'get',
      url: `${FIREBASE_CONFIG.databaseURL}/users.json`,
      params: {
        auth: AccessStore.getToken(),
      },
    })
      .then((response) => {
        dispatch(usersFetchDataSuccess(response.data));
      })
      .catch((error) => {
        dispatch(usersHasErrored(`Error during fetching data ${error.message}`));
      });
  };
}

export function resetUserPassword(userEmail) {
  return (dispatch) => {
    FIREBASE_API_REF.auth()
      .sendPasswordResetEmail(userEmail)
      .then(
        () => {
          dispatch(userPassResetSuccess(`Password reset sent to ${userEmail}`));
        },
        (error) => {
          dispatch(usersHasErrored(error.message));
        },
      );
  };
}

export function addItem(userObj) {
  return (dispatch) => {
    let userId = '';

    axios({
      method: 'get',
      url: `${FIREBASE_CONFIG.databaseURL}/users.json`,
      params: {
        auth: AccessStore.getToken(),
      },
    })
      .then((response) => {
        for (const currUserId in response.data) {
          // if user already exists in database, reset his flag and password
          if (response.data[currUserId].email === userObj.email) {
            userId = currUserId;
            return axios({
              method: 'put',
              url: `${FIREBASE_CONFIG.databaseURL}/users/${currUserId}/active.json`,
              data: JSON.stringify(true),
              params: {
                auth: AccessStore.getToken(),
              },
            }).then(() => FIREBASE_API_REF.auth()
              .sendPasswordResetEmail(userObj.email)
              .then(
                () => {},
                (error) => {
                  dispatch(usersHasErrored(`Error creating user: ${error.message}`));
                },
              ));
          }
        }

        return FIREBASE_API_REF.auth()
          .createUserWithEmailAndPassword(userObj.email, 'default')
          .then(userData => FIREBASE_API_REF.auth()
            .sendPasswordResetEmail(userObj.email)
            .then(() => {
              userId = userData.uid;
            })
            .then(() => axios({
              method: 'put',
              url: `${FIREBASE_CONFIG.databaseURL}/users/${userId}.json`,
              data: JSON.stringify({
                email: userObj.email,
                active: true,
                mod: userObj.mod,
              }),
              params: {
                auth: AccessStore.getToken(),
              },
            })));
      })
      .then(() => {
        userObj.id = userId;
        dispatch(usersAddedSuccess(userObj, `User created ${userObj.email}`));
      })
      .catch((error) => {
        dispatch(usersHasErrored(`Error creating user: ${error.message}`));
      });
  };
}

export function deleteItem(userId) {
  return (dispatch) => {
    axios
      .all([
        axios({
          method: 'get',
          url: `${FIREBASE_CONFIG.databaseURL}/users/${userId}.json`,
          params: {
            auth: AccessStore.getToken(),
          },
        }),
        axios({
          method: 'put',
          url: `${FIREBASE_CONFIG.databaseURL}/users/${userId}/active.json`,
          data: JSON.stringify(false),
          params: {
            auth: AccessStore.getToken(),
          },
        }),
      ])
      .then(
        axios.spread((responseGetUser) => {
          responseGetUser.data.id = userId;

          axios({
            method: 'delete',
            url: `${FIREBASE_CONFIG.databaseURL}/repairs/${userId}.json`,
            params: {
              auth: AccessStore.getToken(),
            },
          }).then(() => {
            dispatch(
              userDeletedSuccess(
                responseGetUser.data,
                `User deleted ${responseGetUser.data.email}`,
              ),
            );
          });
        }),
      )
      .catch((error) => {
        dispatch(usersHasErrored(`Error removing user: ${error.message}`));
      });
  };
}

export function updateItem(userObj) {
  return (dispatch) => {
    // change only role
    if (userObj.oldEmail === userObj.newEmail || userObj.newEmail.length < 1) {
      axios({
        method: 'put',
        url: `${FIREBASE_CONFIG.databaseURL}/users/${userObj.id}.json`,
        data: JSON.stringify({
          email: userObj.oldEmail,
          active: true,
          mod: userObj.mod,
        }),
        params: {
          auth: AccessStore.getToken(),
        },
      })
        .then(() => {
          userObj.email = userObj.oldEmail;
          delete userObj.newEmail;
          dispatch(userUpdateRoleSuccess(userObj, `User role was changed: ${userObj.email}`));
        })
        .catch((error) => {
          dispatch(usersHasErrored(`Error resetting password: ${error.message}`));
        });
    } else {
      // change user email
      FIREBASE_API_REF.auth()
        .createUserWithEmailAndPassword(userObj.newEmail, 'default')
        .then((userData) => {
          FIREBASE_API_REF.auth()
            .sendPasswordResetEmail(userObj.newEmail)
            .then(() => {
              axios({
                method: 'put',
                url: `${FIREBASE_CONFIG.databaseURL}/users/${userObj.id}/active.json`,
                data: JSON.stringify(false),
                params: {
                  auth: AccessStore.getToken(),
                },
              });
            })
            .then(() => {
              axios({
                method: 'put',
                url: `${FIREBASE_CONFIG.databaseURL}/users/${userData.uid}.json`,
                data: JSON.stringify({
                  email: userObj.newEmail,
                  active: true,
                  mod: userObj.mod,
                }),
                params: {
                  auth: AccessStore.getToken(),
                },
              });
            })
            .then(() => {
              axios({
                method: 'get',
                url: `${FIREBASE_CONFIG.databaseURL}/repairs/${userObj.id}/repairs.json`,
                params: {
                  auth: AccessStore.getToken(),
                },
              })
                .then((response) => {
                  const repairs = response.data;

                  for (const repairId in repairs) {
                    repairs[repairId].userId = userData.uid;
                  }

                  axios({
                    method: 'put',
                    url: `${FIREBASE_CONFIG.databaseURL}/repairs/${userData.uid}.json`,
                    params: {
                      auth: AccessStore.getToken(),
                    },
                    data: JSON.stringify({
                      repairs,
                    }),
                  });
                })
                .then((response) => {
                  axios({
                    method: 'delete',
                    url: `${FIREBASE_CONFIG.databaseURL}/repairs/${userObj.id}.json`,
                    params: {
                      auth: AccessStore.getToken(),
                    },
                  });
                })
                .then(() => {
                  userObj.email = userObj.newEmail;
                  delete userObj.oldEmail;
                  delete userObj.newEmail;
                  userObj.newId = userData.uid;
                  dispatch(
                    userUpdateEmailSuccess(userObj, `Email was changed to ${userObj.email}`),
                  );
                });
            })
            .catch((error) => {
              dispatch(usersHasErrored(`Error resetting password: ${error.message}`));
            });
        });
    }
  };
}
