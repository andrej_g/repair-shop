import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './userModal.scss';

export const USER_MODAL_TYPE = {
  CREATE_USER: 'CREATE_USER',
  EDIT_USER: 'EDIT_USER',
};

/**
 * Modal window to add/edit user
 */
export default class UserModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      'new-user-email-input': '',
      'user-old-email-input': '',
      selectUserGroup: '',
    };

    this.handleSubmitCreate = this.handleSubmitCreate.bind(this);
    this.handleSubmitEdit = this.handleSubmitEdit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleChangeSelect = this.handleChangeSelect.bind(this);
  }

  componentDidMount() {
    const that = this;
    $('#user-modal').on('hidden.bs.modal', () => {
      that.setState({
        'new-user-email-input': '',
        'user-old-email-input': '',
        selectUserGroup: '',
      });
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.userEmail) {
      this.setState({
        'new-user-email-input': nextProps.userEmail,
        'user-old-email-input': nextProps.userEmail,
      });
    }
  }

  componentWillUnmount() {
    $('#user-modal').data('bs.modal', null);
  }

  handleSubmitCreate(e) {
    console.log('modal submit create');
    e.preventDefault();
    const userObj = {};
    userObj.email = ReactDOM.findDOMNode(document.getElementById('new-user-email-input')).value;
    if (ReactDOM.findDOMNode(document.getElementById('role-picker-new')).value === 'User') {
      userObj.mod = false;
    } else {
      userObj.mod = true;
    }
    this.props.onCreateUser(userObj, this.props.type);
    return false;
  }

  handleSubmitEdit(e) {
    e.preventDefault();
    const userObj = {};
    userObj.oldEmail = ReactDOM.findDOMNode(document.getElementById('user-old-email-input')).value;
    userObj.newEmail = ReactDOM.findDOMNode(document.getElementById('user-new-email-input')).value;
    if (ReactDOM.findDOMNode(document.getElementById('role-picker-edit')).value === 'User') {
      userObj.mod = false;
    } else {
      userObj.mod = true;
    }
    this.props.onEditUser(userObj);
    return false;
  }

  handleInputChange(e) {
    const nextState = {};
    nextState[e.target.name] = e.target.value;
    this.setState(nextState);
  }

  handleChangeSelect(e) {
    this.setState({ selectUserGroup: e.target.value });
  }

  render() {
    let modalContent = null;

    let newUserEmailVal = '';
    let editEmailOldEmailVal = '';
    let userGroup = '';
    if (this.props.userEmail) {
      newUserEmailVal = this.props.userEmail;
      editEmailOldEmailVal = this.props.userEmail;
    }

    if (this.state['new-user-email-input'].length > 0) {
      newUserEmailVal = this.state['new-user-email-input'];
    }

    if (this.state['user-old-email-input'].length > 0) {
      editEmailOldEmailVal = this.state['user-old-email-input'];
    }

    if (this.props.mod) {
      userGroup = 'Moderator';
    } else {
      userGroup = 'User';
    }

    if (this.state.selectUserGroup.length > 0) {
      userGroup = this.state.selectUserGroup;
    }

    const modalFooter = (
      <div className="modal-footer">
        <button type="submit" className="btn btn-primary">
          Save
        </button>
        <button type="button" className="btn btn-default" data-dismiss="modal">
          Close
        </button>
      </div>
    );

    if (this.props.type === USER_MODAL_TYPE.CREATE_USER) {
      modalContent = (
        <div className="modal-content">
          <form onSubmit={this.handleSubmitCreate.bind(this)}>
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"><i className="fa fa-times" aria-hidden="true" /></span>
              </button>
              <h4 className="modal-title" id="myModalLabel">
                Add User
              </h4>
            </div>
            <div className="modal-body">
              <div className="form-group">
                <label htmlFor="new-user-email-input">Email</label>
                <input
                  type="email"
                  className="form-control"
                  id="new-user-email-input"
                  name="new-user-email-input"
                  placeholder="Email"
                  required
                  onChange={this.handleInputChange.bind(this)}
                  value={newUserEmailVal}
                />
              </div>
              <div className="form-group">
                <label htmlFor="role-picker-new">Role</label>
                <select
                  id="role-picker-new"
                  className="form-control"
                  value={userGroup}
                  onChange={this.handleChangeSelect}
                >
                  <option value="User">User</option>
                  <option value="Moderator">Moderator</option>
                </select>
              </div>
            </div>
            {modalFooter}
          </form>
        </div>
      );
    } else {
      modalContent = (
        <div className="modal-content">
          <form onSubmit={this.handleSubmitEdit.bind(this)}>
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"><i className="fa fa-times" aria-hidden="true" /></span>
              </button>
              <h4 className="modal-title" id="myModalLabel">
                Edit User
              </h4>
            </div>
            <div className="modal-body">
              <div className="form-group">
                <label htmlFor="user-old-email-input">Old email</label>
                <input
                  type="email"
                  className="form-control"
                  id="user-old-email-input"
                  name="user-old-email-input"
                  placeholder="Email"
                  required
                  onChange={this.handleInputChange}
                  value={editEmailOldEmailVal}
                />
              </div>
              <div className="form-group">
                <label htmlFor="user-new-email-input">New email</label>
                <input
                  type="email"
                  className="form-control"
                  id="user-new-email-input"
                  placeholder="Email"
                />
              </div>
              <div className="form-group">
                <label htmlFor="role-picker-edit">Role</label>
                <select
                  id="role-picker-edit"
                  className="form-control"
                  value={userGroup}
                  onChange={this.handleChangeSelect}
                >
                  <option>User</option>
                  <option>Moderator</option>
                </select>
              </div>
            </div>
            {modalFooter}
          </form>
        </div>
      );
    }

    return (
      <div
        className="modal fade"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="newItemModalLabel"
        id="user-modal"
      >
        <div className="modal-dialog modal-lg" role="document">
          {modalContent}
        </div>
      </div>
    );
  }
}
