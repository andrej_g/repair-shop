import React, { Component } from 'react';

/**
 * Table row component for react-bootstrap-table
 */
export default class TableRowButtons extends Component {
  constructor(props) {
    super(props);

    this.handleDeleteClick = this.handleDeleteClick.bind(this);
    this.handleResetClick = this.handleResetClick.bind(this);
    this.handleEditClick = this.handleEditClick.bind(this);
  }

  handleDeleteClick(e) {
    this.props.onDeleteClick(this.props.rowContent);
    e.preventDefault();
    e.stopPropagation();
  }

  handleResetClick(e) {
    this.props.onResetClick(this.props.rowContent);
    e.preventDefault();
    e.stopPropagation();
  }

  handleEditClick(e) {
    this.props.onEditClick(this.props.rowContent);
    e.preventDefault();
    e.stopPropagation();
  }

  render() {
    return (
      <div className="row-container">
        <div className="row-text-container">{this.props.cellContent}</div>
        <div className="row-buttons-container">
          <button className="btn btn-danger btn-xs" onClick={this.handleDeleteClick}>
            Delete
          </button>
          <button className="btn btn-warning btn-xs" onClick={this.handleResetClick}>
            Reset Password
          </button>
          <button className="btn btn-info btn-xs" onClick={this.handleEditClick}>
            Edit
          </button>
        </div>
      </div>
    );
  }
}
