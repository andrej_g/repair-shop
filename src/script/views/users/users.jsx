import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import './users.scss';

import '../../../../node_modules/react-bootstrap-table/css/react-bootstrap-table.css';

import Header from '../../components/header.jsx';
import TableRowButtons from './components/tableRowButtons.jsx';
import UserModal, { USER_MODAL_TYPE } from './components/UserModal.jsx';
import { getAllUsers, addItem, deleteItem, updateItem, resetUserPassword } from './actions';
import { notify } from 'bootstrap-notify';

/**
 * Component represents Users view
 */
class Users extends Component {
  constructor(props) {
    super(props);

    this.state = {
      users: {},
      selectedUser: {},
      modalType: USER_MODAL_TYPE.CREATE_USER,
      errorMessage: '',
      successMessage: '',
    };

    this.handleEditClick = this.handleEditClick.bind(this);
    this.handleResetClick = this.handleResetClick.bind(this);
    this.handleDeleteClick = this.handleDeleteClick.bind(this);
    this.handleCreateClick = this.handleCreateClick.bind(this);
    this.handleCreateUser = this.handleCreateUser.bind(this);
    this.handleEditUser = this.handleEditUser.bind(this);
  }

  componentDidMount() {
    this.props.getUsers();
  }

  componentWillReceiveProps(newProps) {
    if (newProps.users !== this.props.users) {
      this.setState({ successMessage: newProps.users.successMessage });
    }
    if (newProps.usersHasErrored !== this.props.usersHasErrored) {
      this.setState({ errorMessage: newProps.usersHasErrored.errorMessage });
    }
  }

  handleDeleteClick(rowContent) {
    this.props.deleteUser(rowContent.id);
  }

  handleCreateClick() {
    this.setState({ selectedUser: {}, modalType: USER_MODAL_TYPE.CREATE_USER });
    $('#user-modal').modal();
  }

  handleResetClick(rowContent) {
    this.props.resetUserPassword(rowContent.email);
  }

  handleCreateUser(userObj) {
    this.props.addUser(userObj);
  }

  handleEditUser(userObj) {
    userObj.id = this.state.selectedUser.id;
    this.props.updateUser(userObj);
  }

  handleEditClick(rowContent) {
    this.setState({ selectedUser: rowContent, modalType: USER_MODAL_TYPE.EDIT_USER });
    $('#user-modal').modal();
  }

  render() {
    const that = this;
    const usersInArray =
      this.props.users.userList && Object.keys(this.props.users.userList).length > 0
        ? this.props.users.userList.toArray()
        : [];

    if (this.state.errorMessage.length > 0) {
      const notify = $.notify(`<strong>Error</strong> ${this.state.errorMessage}`, {
        animate: {
          enter: 'animated fadeInRight',
          exit: 'animated fadeOutRight',
        },
        allow_dismiss: true,
        showProgressbar: false,
        delay: 2500,
        type: 'danger',
        z_index: 2000,
      });

      setTimeout(() => {
        that.setState({ errorMessage: '', successMessage: '' });
        $.notifyClose('top-right');
      }, 2500);
    }

    if (this.state.successMessage && this.state.successMessage.length > 0) {
      $('#user-modal').modal('hide');
      const notify = $.notify(`<strong>Success</strong> ${this.state.successMessage}`, {
        animate: {
          enter: 'animated fadeInRight',
          exit: 'animated fadeOutRight',
        },
        allow_dismiss: true,
        showProgressbar: false,
        delay: 2500,
        type: 'success',
      });

      setTimeout(() => {
        that.setState({ errorMessage: '', successMessage: '' });
        $.notifyClose('top-right');
      }, 2500);
    }

    // row formatter for react bootstrap table
    function rowFormatter(cell, row) {
      return (
        <TableRowButtons
          cellContent={cell}
          rowContent={row}
          onDeleteClick={that.handleDeleteClick}
          onResetClick={that.handleResetClick}
          onEditClick={that.handleEditClick}
        />
      );
    }

    // reinitialize hover event on row after table reinitialization
    function onAfterTableComplete() {
      $('.react-bs-container-body table tr').off('mouseenter mouseleave');
      $('.react-bs-container-body table tr').hover(
        function () {
          $(this)
            .find('button')
            .css('visibility', 'visible');
        },
        function () {
          $(this)
            .find('button')
            .css('visibility', 'hidden');
        },
      );
    }

    // table options
    const options = {
      afterTableComplete: onAfterTableComplete,
    };

    return (
      <div className="users-container">
        <Header />
        <h3>Users</h3>
        <button
          className="btn create-button icon-button btn-primary btn-sm"
          onClick={this.handleCreateClick}
        >
          <i className="fa fa-plus" aria-hidden="true" />
          <span>Add user</span>
        </button>
        <BootstrapTable
          data={usersInArray}
          striped
          hover
          condensed
          pagination
          search
          options={options}
        >
          <TableHeaderColumn
            isKey
            dataField="email"
            width="200"
            dataSort
            dataFormat={rowFormatter}
          >
            Email
          </TableHeaderColumn>
        </BootstrapTable>
        <UserModal
          onCreateUser={this.handleCreateUser}
          onEditUser={this.handleEditUser}
          userEmail={this.state.selectedUser.email}
          mod={this.state.selectedUser.mod}
          type={this.state.modalType}
        />
      </div>
    );
  }
}

Users.propTypes = {
  getUsers: PropTypes.func.isRequired,
  addUser: PropTypes.func.isRequired,
  updateUser: PropTypes.func.isRequired,
  deleteUser: PropTypes.func.isRequired,
  resetUserPassword: PropTypes.func.isRequired,
  users: PropTypes.object.isRequired,
  usersHasErrored: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  users: state.rootReducer.users,
  usersHasErrored: state.rootReducer.usersHasErrored,
});

const mapDispatchToProps = dispatch => ({
  getUsers: () => dispatch(getAllUsers()),
  addUser: newItem => dispatch(addItem(newItem)),
  updateUser: itemObj => dispatch(updateItem(itemObj)),
  deleteUser: itemId => dispatch(deleteItem(itemId)),
  resetUserPassword: itemId => dispatch(resetUserPassword(itemId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Users);
