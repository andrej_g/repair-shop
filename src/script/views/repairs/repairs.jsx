import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { itemsFetchData, addItem, deleteItem, updateItem, filterItems } from './actions';
import AccessStore from '../access/AccessStore';
import './repairs.scss';

import Header from '../../components/header.jsx';
import RepairModal from './components/repairModal.jsx';
import RepairItem from './components/repairItem.jsx';
import RepairsFilter from './components/repairsFilter.jsx';

class Repairs extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedRepair: {},
      selectedUserId: '',
    };

    this.handleCreateClick = this.handleCreateClick.bind(this);
    this.editItem = this.editItem.bind(this);
    this.editClicked = this.editClicked.bind(this);
    this.deleteClicked = this.deleteClicked.bind(this);
    this.filterRepairs = this.filterRepairs.bind(this);
    this.resetFilter = this.resetFilter.bind(this);
    this.changeRepairState = this.changeRepairState.bind(this);
  }

  componentDidMount() {
    if (AccessStore.isModerator() && this.state.selectedUserId.length < 1) {
      this.props.getRepairItems();
    } else if (this.state.selectedUserId.length > 1) {
      this.props.getRepairItems(this.state.selectedUserId);
    } else {
      this.props.getRepairItems(AccessStore.getId());
    }
  }

  handleCreateClick() {
    this.setState({ selectedRepair: {} });
    $('#repair-modal').modal();
  }

  editClicked(data) {
    this.setState({ selectedRepair: data });
    $('#repair-modal').modal();
  }

  deleteClicked(data) {
    const currData = data;
    if (AccessStore.isModerator()) {
      const userId = currData.userId;
      delete currData.userId;
      this.props.deleteRepair(currData.id, userId);
    } else {
      this.props.deleteRepair(currData.id);
    }
  }

  filterRepairs(filterObj) {
    const that = this;
    this.setState({ selectedUserId: filterObj.userId }, () => {
      if (AccessStore.isModerator() && that.state.selectedUserId.length < 1) {
        that.props.filterRepairs(filterObj);
      } else if (that.state.selectedUserId && that.state.selectedUserId.length > 1) {
        that.props.filterRepairs(filterObj, that.state.selectedUserId);
      } else {
        that.props.filterRepairs(filterObj, AccessStore.getId());
      }
    });
  }

  changeRepairState(repairObj, completed, pending) {
    const currRepairObj = repairObj;
    currRepairObj.completed = completed;
    currRepairObj.pending = pending;
    if (currRepairObj.userId) {
      const userId = currRepairObj.userId;
      this.props.updateRepair(currRepairObj, userId);
    } else {
      this.props.updateRepair(currRepairObj);
    }
  }

  editItem(newItem) {
    const currNewItem = newItem;
    if (currNewItem.id) {
      if (currNewItem.userId) {
        const userId = currNewItem.userId;
        delete currNewItem.userId;
        this.props.updateRepair(currNewItem, userId);
      } else {
        this.props.updateRepair(currNewItem);
      }
    } else {
      currNewItem.completed = false;
      if (currNewItem.userId) {
        const userId = currNewItem.userId;
        delete currNewItem.userId;
        this.props.addRepair(currNewItem, userId);
      } else {
        this.props.addRepair(currNewItem);
      }
    }
    $('#repair-modal').modal('hide');
  }

  resetFilter() {
    if (AccessStore.isModerator() && this.state.selectedUserId.length < 1) {
      this.props.getRepairItems();
    } else if (this.state.selectedUserId && this.state.selectedUserId.length > 1) {
      this.props.getRepairItems(this.state.selectedUserId);
    } else {
      this.props.getRepairItems(AccessStore.getId());
    }
  }

  render() {
    if (this.props.hasErrored.error) {
      const notify = $.notify(`<strong>Error</strong> ${this.props.hasErrored.message}`, {
        animate: {
          enter: 'animated fadeInRight',
          exit: 'animated fadeOutRight',
        },
        allow_dismiss: true,
        showProgressbar: false,
        delay: 2500,
        type: 'danger',
        z_index: 2000,
      });

      setTimeout(() => {
        $.notifyClose('top-right');
      }, 2500);
    }

    const that = this;

    let repairs = <h4>0 repairs</h4>;
    if (this.props.repairs.repairsList && this.props.repairs.repairsList.size > 0) {
      repairs = this.props.repairs.repairsList.map((repairObj, i) => {
        let footerSpan = null;
        const repairsLabel = that.props.repairs.repairsList.size > 1 ? ' repairs' : ' repair';
        if (i === that.props.repairs.repairsList.size - 1) {
          footerSpan = (
            <span className="footer-repair-span">
              {that.props.repairs.repairsList.size.toString() + repairsLabel}
            </span>
          );
        }

        return (
          <RepairItem
            key={repairObj.id}
            data={repairObj}
            onRepairClick={this.editClicked}
            onRepairDelete={this.deleteClicked}
            onChangeRepairState={this.changeRepairState}
            footerSpan={footerSpan}
            editable
            isMod={AccessStore.isModerator()}
          />
        );
      });
    }

    return (
      <div className="repairs-container">
        <Header />
        <div className="container-fluid">
          <div className="row">
            <RepairsFilter
              onFilterRepairs={this.filterRepairs}
              onResetFilter={this.resetFilter}
              isMod={AccessStore.isModerator()}
            />
            <div className="col-md-1 wrapper-create">
              <i
                className="fa fa-plus-square fa-3x"
                aria-hidden="true"
                onClick={this.handleCreateClick}
              />
            </div>
          </div>
          <h3>Repairs</h3>
          <div className="list-group">{repairs}</div>
        </div>
        <RepairModal
          onEditItem={this.editItem}
          isMod={AccessStore.isModerator()}
          ref="modal"
          data={this.state.selectedRepair}
        />
      </div>
    );
  }
}

Repairs.propTypes = {
  getRepairItems: PropTypes.func.isRequired,
  addRepair: PropTypes.func.isRequired,
  updateRepair: PropTypes.func.isRequired,
  deleteRepair: PropTypes.func.isRequired,
  filterRepairs: PropTypes.func.isRequired,
  repairs: PropTypes.object.isRequired,
  hasErrored: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  repairs: state.rootReducer.repairs,
  hasErrored: state.rootReducer.repairsHasErrored,
});

const mapDispatchToProps = dispatch => ({
  getRepairItems: userId => dispatch(itemsFetchData(userId)),
  addRepair: (newItem, userId) => dispatch(addItem(newItem, userId)),
  updateRepair: (itemObj, userId) => dispatch(updateItem(itemObj, userId)),
  deleteRepair: (itemId, userId) => dispatch(deleteItem(itemId, userId)),
  filterRepairs: (filterObj, userId) => dispatch(filterItems(filterObj, userId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Repairs);
