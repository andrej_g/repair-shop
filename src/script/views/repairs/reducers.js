import Immutable from 'immutable';
import moment from 'moment';
import { sortByDateDesc } from '../../utils';
import { DATE_FORMAT } from '../../globalConstants';

export function repairsHasErrored(state = {}, action) {
  switch (action.type) {
    case 'ITEMS_HAS_ERRORED':
      return {
        error: action.hasErrored,
        message: action.errorMessage,
      };
    case 'ITEMS_REMOVE_ERROR':
      return {
        error: false,
      };
    default:
      return state;
  }
}

function filterRepairs(action, items) {
  const tmpRepairs = [];
  if (items) {
    Object.keys(items).forEach((repairId) => {
      if (Object.prototype.hasOwnProperty.call(items, repairId)) {
        let validItem = false;
        if (moment(action.filterObj.date, DATE_FORMAT, true).isValid()) {
          if (items[repairId].date === action.filterObj.date) {
            validItem = true;
          } else {
            validItem = false;
            return;
          }
        }
        if (action.filterObj.time.length > 0) {
          if (items[repairId].time === action.filterObj.time) {
            validItem = true;
          } else {
            validItem = false;
            return;
          }
        }
        if (action.filterObj.pending && items[repairId].pending) {
          validItem = true;
        } else if (action.filterObj.pending && !items[repairId].pending) {
          validItem = false;
          return;
        }
        if (action.filterObj.complete && action.filterObj.incomplete) {
          validItem = true;
        } else if (items[repairId].completed && action.filterObj.complete) {
          validItem = true;
        } else if (!items[repairId].completed && action.filterObj.incomplete) {
          validItem = true;
        } else {
          validItem = false;
          return;
        }
        if (validItem) {
          const currItem = items[repairId];
          currItem.id = repairId;
          tmpRepairs.push(currItem);
        }
      }
    });
  }
  return tmpRepairs;
}

export function repairs(state = {}, action) {
  switch (action.type) {
    case 'ITEMS_FETCH_DATA_SUCCESS': {
      const tempRepairs = [];

      if (action.userId) {
        if (action.items) {
          Object.keys(action.items).forEach((repairId) => {
            if (Object.prototype.hasOwnProperty.call(action.items, repairId)) {
              const currItem = action.items[repairId];
              currItem.id = repairId;
              currItem.userId = action.userId;
              tempRepairs.push(currItem);
            }
          });
        }
      } else if (action.items) {
        Object.keys(action.items).forEach((userId) => {
          if (Object.prototype.hasOwnProperty.call(action.items, userId)) {
            if (action.items[userId].repairs) {
              Object.keys(action.items[userId].repairs).forEach((repairId) => {
                if (Object.prototype.hasOwnProperty.call(action.items[userId].repairs, repairId)) {
                  const currItem = action.items[userId].repairs[repairId];
                  currItem.id = repairId;
                  currItem.userId = userId;
                  tempRepairs.push(currItem);
                }
              });
            }
          }
        });
      }

      tempRepairs.sort(sortByDateDesc);
      return {
        repairsList: Immutable.List(tempRepairs),
      };
    }
    case 'REPAIRS_FILTER_SUCCESS': {
      let tmpRepairs = [];
      if (action.filterObj.userId) {
        tmpRepairs = filterRepairs(action, action.items);
      } else if (action.items) {
        if (action.items) {
          Object.keys(action.items).forEach((userCurrId) => {
            if (Object.prototype.hasOwnProperty.call(action.items, userCurrId)) {
              if (action.items[userCurrId].repairs) {
                tmpRepairs = tmpRepairs.concat(
                  filterRepairs(action, action.items[userCurrId].repairs),
                );
              } else {
                tmpRepairs = filterRepairs(action, action.items);
              }
            }
          });
        }
      }
      tmpRepairs.sort(sortByDateDesc);
      return {
        repairsList: Immutable.List(tmpRepairs),
      };
    }
    case 'REPAIR_ADDED_SUCCESS': {
      let currStateRepairList = state.repairsList;
      currStateRepairList = currStateRepairList.push(action.repairObj).sort(sortByDateDesc);
      return {
        repairsList: Immutable.List(currStateRepairList),
      };
    }
    case 'REPAIR_DELETED_SUCCESS': {
      let currStateRepairList = state.repairsList;
      for (let i = 0; i < currStateRepairList.size; i += 1) {
        if (currStateRepairList.get(i).id.toString() === action.repairId) {
          currStateRepairList = currStateRepairList.delete(i);
          break;
        }
      }
      return {
        repairsList: Immutable.List(currStateRepairList),
      };
    }
    case 'REPAIR_UPDATE_SUCCESS': {
      let currStateRepairList = state.repairsList;
      for (let i = 0; i < currStateRepairList.size; i += 1) {
        if (currStateRepairList.get(i).id.toString() === action.repairObj.id) {
          const currAction = action;
          if (action.userId) {
            currAction.repairObj.userId = currAction.userId;
          }
          currStateRepairList = currStateRepairList.update(i, () => currAction.repairObj);
          break;
        }
      }
      currStateRepairList = currStateRepairList.sort(sortByDateDesc);
      return {
        repairsList: Immutable.List(currStateRepairList),
      };
    }
    default:
      return state;
  }
}
