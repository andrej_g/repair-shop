import axios from 'axios';
import moment from 'moment';
import { FIREBASE_CONFIG, DATE_FORMAT } from '../../globalConstants';
import AccessStore from '../access/AccessStore';

export function itemsHasErrored(bool, errorMessage) {
  return {
    type: 'ITEMS_HAS_ERRORED',
    hasErrored: bool,
    errorMessage,
  };
}

export function removeItemsError() {
  return {
    type: 'ITEMS_HAS_ERRORED',
  };
}

export function itemsIsLoading(bool) {
  return {
    type: 'ITEMS_IS_LOADING',
    isLoading: bool,
  };
}

export function itemsFetchDataSuccess(items, userId) {
  return {
    type: 'ITEMS_FETCH_DATA_SUCCESS',
    items,
    userId,
  };
}

export function repairAddedSuccess(repairObj) {
  return {
    type: 'REPAIR_ADDED_SUCCESS',
    repairObj,
  };
}

export function repairDeletedSuccess(repairId) {
  return {
    type: 'REPAIR_DELETED_SUCCESS',
    repairId,
  };
}

export function repairUpdateSuccess(repairObj, userId) {
  return {
    type: 'REPAIR_UPDATE_SUCCESS',
    repairObj,
    userId,
  };
}

export function repairsFilterSuccess(items, filterObj) {
  return {
    type: 'REPAIRS_FILTER_SUCCESS',
    filterObj,
    items,
  };
}

function checkTimeOverlap(state, repairObj, repairId) {
  const valid = true;
  for (let i = 0; i < state.rootReducer.repairs.repairsList.size; i += 1) {
    const currDate = moment(state.rootReducer.repairs.repairsList.get(i).date, DATE_FORMAT);
    const currTime = moment(state.rootReducer.repairs.repairsList.get(i).time, ['h:m a', 'H:m']);
    const currOneHourLess = moment(state.rootReducer.repairs.repairsList.get(i).time, [
      'h:m a',
      'H:m',
    ]).subtract(1, 'hours');
    const currOneHourMore = moment(state.rootReducer.repairs.repairsList.get(i).time, [
      'h:m a',
      'H:m',
    ]).add(1, 'hours');

    const checkExistingItem = repairId && repairId.length > 0 ? true : false;

    if ((checkExistingItem && state.rootReducer.repairs.repairsList.get(i).id !== repairId) &&
      currDate.isSame(moment(repairObj.date, DATE_FORMAT)) &&
      ((moment(repairObj.time, ['h:m a', 'H:m']).isAfter(currOneHourLess) &&
        moment(repairObj.time, ['h:m a', 'H:m']).isBefore(currTime)) ||
        (moment(repairObj.time, ['h:m a', 'H:m']).isBefore(currOneHourMore) &&
          moment(repairObj.time, ['h:m a', 'H:m']).isAfter(currTime)))
    ) {
      return false;
    }
  }
  return valid;
}

export function itemsFetchData(userId) {
  return (dispatch) => {
    dispatch(itemsIsLoading(true));

    let url = `${FIREBASE_CONFIG.databaseURL}/repairs`;

    if (userId) {
      url += `/${userId}/repairs.json`;
    } else {
      url += '.json';
    }

    axios({
      method: 'get',
      url,
      params: {
        auth: AccessStore.getToken(),
      },
    })
      .then((response) => {
        dispatch(removeItemsError());
        dispatch(itemsFetchDataSuccess(response.data, userId));
      })
      .catch((error) => {
        dispatch(itemsHasErrored(true, `Error during fetching data ${error.message}`));
      });
  };
}

export function filterItems(filterObj, userId) {
  return (dispatch) => {
    let url = `${FIREBASE_CONFIG.databaseURL}/repairs`;

    if (userId) {
      url += `/${userId}/repairs.json`;
    } else {
      url += '.json';
    }

    axios({
      method: 'get',
      url,
      params: {
        auth: AccessStore.getToken(),
      },
    })
      .then((response) => {
        dispatch(removeItemsError());
        dispatch(repairsFilterSuccess(response.data, filterObj));
      })
      .catch((error) => {
        dispatch(itemsHasErrored(true, `Error during fetching data ${error.message}`));
      });
  };
}

export function addItem(repairObj, userId) {
  const userIdParam = userId || AccessStore.getId();
  return (dispatch, getState) => {
    if (checkTimeOverlap(getState(), repairObj)) {
      const url = `${FIREBASE_CONFIG.databaseURL}/repairs/${userIdParam}/repairs.json`;
      axios({
        method: 'post',
        url,
        data: JSON.stringify(repairObj),
        params: {
          auth: AccessStore.getToken(),
        },
      })
        .then((response) => {
          const currRepairObj = repairObj;
          currRepairObj.id = response.data.name;
          dispatch(repairAddedSuccess(currRepairObj));
        })
        .catch((error) => {
          dispatch(removeItemsError());
          dispatch(itemsHasErrored(true, `Error during adding data ${error.message}`));
        });
    } else {
      dispatch(itemsHasErrored(true, 'Times overlap'));
    }
  };
}

export function deleteItem(repairId, userId) {
  const userIdParam = userId || AccessStore.getId();
  return (dispatch) => {
    axios({
      method: 'delete',
      url:
        `${FIREBASE_CONFIG.databaseURL}/repairs/${userIdParam}/repairs/${repairId}.json`,
      params: {
        auth: AccessStore.getToken(),
      },
    })
      .then(() => {
        dispatch(removeItemsError());
        dispatch(repairDeletedSuccess(repairId));
      })
      .catch((error) => {
        dispatch(itemsHasErrored(true, `Error during deleting data ${error.message}`));
      });
  };
}

export function updateItem(repairObj, userId) {
  const currRepairObj = repairObj;
  const userIdParam = userId || AccessStore.getId();
  const repairId = currRepairObj.id;
  delete currRepairObj.id;
  delete currRepairObj.userId;
  return (dispatch, getState) => {
    if (checkTimeOverlap(getState(), currRepairObj, repairId)) {
      axios({
        method: 'put',
        url:
          `${FIREBASE_CONFIG.databaseURL
          }/repairs/${
            userIdParam
          }/repairs/${
            repairId
          }.json`,
        data: JSON.stringify(currRepairObj),
        params: {
          auth: AccessStore.getToken(),
        },
      })
        .then(() => {
          currRepairObj.id = repairId;
          dispatch(removeItemsError());
          dispatch(repairUpdateSuccess(currRepairObj, userId));
        })
        .catch((error) => {
          dispatch(itemsHasErrored(true, `Error during updating data ${error.message}`));
        });
    } else {
      dispatch(itemsHasErrored(true, 'Times overlap'));
    }
  };
}
