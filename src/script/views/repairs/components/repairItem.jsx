import React, { Component } from 'react';
import './repairItem.scss';

/**
 * Component represents single repair element in list of repairs
 */
export default class RepairItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      completed: this.props.data.completed,
      pending: this.props.data.pending,
    };

    this.handleDeleteClick = this.handleDeleteClick.bind(this);
    this.handleEditClick = this.handleEditClick.bind(this);
    this.toggleCheckbox = this.toggleCheckbox.bind(this);
  }

  handleDeleteClick(e) {
    this.props.onRepairDelete(this.props.data);
    e.stopPropagation();
  }

  handleEditClick() {
    this.props.onRepairClick(this.props.data);
  }

  toggleCheckbox(e) {
    const that = this;

    if (e.target.id === 'repair-approve-checkbox') {
      this.setState({ pending: !this.state.pending }, () => {
        that.props.onChangeRepairState(that.props.data, that.state.completed, that.state.pending);
      });
    }

    if (e.target.id === 'repair-completed-checkbox' && !this.props.isMod && !this.state.completed) {
      this.setState({ completed: !this.state.completed, pending: true }, () => {
        that.props.onChangeRepairState(that.props.data, that.state.completed, that.state.pending);
      });
    } else if (e.target.id === 'repair-completed-checkbox' && this.props.isMod) {
      this.setState({ completed: !this.state.completed }, () => {
        that.props.onChangeRepairState(that.props.data, that.state.completed, that.state.pending);
      });
    }
  }

  render() {
    let approve = null;
    let completed = (
      <label htmlFor="repair-completed-checkbox">
        <input
          type="checkbox"
          className="repair-checkbox"
          id="repair-completed-checkbox"
          disabled={this.state.completed}
          checked={this.state.completed}
          onChange={this.toggleCheckbox}
        />Completed
      </label>
    );

    if (this.props.isMod) {
      if (this.props.data.pending) {
        approve = (
          <label htmlFor="repair-approve-checkbox">
            <input
              type="checkbox"
              className="repair-checkbox"
              id="repair-approve-checkbox"
              checked={!this.state.pending}
              onChange={this.toggleCheckbox}
            />Approve
          </label>
        );
      }

      completed = (
        <label htmlFor="repair-completed-checkbox">
          <input
            type="checkbox"
            className="repair-checkbox"
            id="repair-completed-checkbox"
            checked={this.state.completed}
            onChange={this.toggleCheckbox}
          />Completed
        </label>
      );
    }

    const editButtons = this.props.editable ? (
      <div className="edit-buttons">
        {completed}
        {approve}
        <button className="btn btn-xs edit close" ref="edit" onClick={this.handleEditClick}>
          <i className="fa fa-pencil" aria-hidden="true" />
        </button>
        <button
          className="btn btn-xs btn-delete close"
          ref="delete"
          onClick={this.handleDeleteClick}
        >
          <i className="fa fa-times" aria-hidden="true" />
        </button>
      </div>
    ) : null;

    return (
      <div className="repair-wrapper list-group-item">
        <div className="repair-element">
          <span className="date">{`${this.props.data.date} ${this.props.data.time}`}</span>
        </div>
        <div className="repair-element">
          <p className="comment">
            <i className="fa fa-comment" aria-hidden="true" />
            {` ${this.props.data.comment}`}
          </p>
        </div>
        {editButtons}
        {this.props.footerSpan}
      </div>
    );
  }
}
