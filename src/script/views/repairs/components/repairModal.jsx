import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import _ from 'lodash';
import moment from 'moment';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { DATE_FORMAT } from '../../../globalConstants';
import { getAllUsers } from '../../users/actions';
import './repairModal.scss';


/**
 * Modal window to add/edit expense
 */
class RepairModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      date: {},
      time: null,
      comment: null,
      selectedUserId: '',
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleChangeSelect = this.handleChangeSelect.bind(this);
  }

  componentDidMount() {
    const that = this;
    $('#repair-modal').on('hidden.bs.modal', () => {
      that.setState({ date: {}, time: null, comment: null });
    });
  }

  componentWillUnmount() {
    $('#repair-modal').data('bs.modal', null);
  }

  handleDateChange(e) {
    this.setState({ date: e });
  }

  handleInputChange(e) {
    const nextState = {};
    nextState[e.target.name] = e.target.value;
    this.setState(nextState);
  }

  handleSubmit(e) {
    e.preventDefault();
    const repairObj = {};
    repairObj.date = ReactDOM.findDOMNode(document.getElementById('date')).value;
    repairObj.time = ReactDOM.findDOMNode(document.getElementById('time')).value;
    repairObj.comment = ReactDOM.findDOMNode(document.getElementById('comment')).value;
    repairObj.completed = false;
    repairObj.pending = false;
    if (this.props.data.id) {
      repairObj.id = this.props.data.id;
    }
    if (this.props.isMod) {
      repairObj.userId = ReactDOM.findDOMNode(document.getElementById('modal-select-user')).value;
    }
    this.props.onEditItem(repairObj);
    return false;
  }

  handleChangeSelect(e) {
    const userId = e.target.value;
    this.setState({ selectedUserId: userId });
  }

  render() {
    const that = this;
    let selectedDate = moment();
    let timeVal = '00:00';
    let commentVal = '';

    if (_.get(this.props.data, 'date')) {
      selectedDate = moment(this.props.data.date, DATE_FORMAT);
    }

    if (Object.keys(this.state.date).length > 0) {
      selectedDate = this.state.date;
    }

    if (_.get(this.props.data, 'time')) {
      timeVal = this.props.data.time;
    }

    if (this.state.time !== null) {
      timeVal = this.state.time;
    }

    if (_.get(this.props.data, 'comment')) {
      commentVal = this.props.data.comment;
    }

    if (this.state.comment !== null) {
      commentVal = this.state.comment;
    }

    let usersSelect = null;

    if (this.props.isMod) {
      const usersInArray =
        this.props.users.userList && Object.keys(this.props.users.userList).length > 0
          ? this.props.users.userList.toArray()
          : [];

      const options = usersInArray.map((user) => {
        if (_.get(that.props.data, 'userId') && that.props.data.userId === user.id) {
          return (
            <option key={user.id} value={user.id} selected>
              {user.email}
            </option>
          );
        }
        return (
          <option key={user.id} value={user.id}>
            {user.email}
          </option>
        );
      });

      usersSelect = (
        <div className="form-group">
          <label htmlFor="usersSelect">Users</label>
          <select
            id="modal-select-user"
            name="usersSelect"
            className="form-control"
            onChange={this.handleChangeSelect.bind(this)}
          >
            {options}
          </select>
        </div>
      );
    }

    const headerStartLabel = Object.keys(this.props.data).length > 0 ? 'Edit' : 'Create';

    return (
      <div
        className="modal fade"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="newItemModalLabel"
        id="repair-modal"
      >
        <div className="modal-dialog modal-lg" role="document">
          <div className="modal-content">
            <form onSubmit={this.handleSubmit}>
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true"><i className="fa fa-times" aria-hidden="true" /></span>
                </button>
                <h4 className="modal-title">{`${headerStartLabel} repair`}</h4>
              </div>
              <div className="modal-body">
                <div className="form-group">
                  <label htmlFor="date">Date (d/m/y)</label>
                  <DatePicker
                    selected={selectedDate}
                    onChange={this.handleDateChange}
                    id="date"
                    ref="date"
                    className="form-control"
                    popoverAttachment="bottom center"
                    popoverTargetAttachment="bottom center"
                    popoverTargetOffset="0px 0px"
                    showYearDropdown
                    dateFormatCalendar="MMMM"
                    locale="en-gb"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="time">Time (hh:mm)</label>
                  <input
                    type="time"
                    className="form-control"
                    id="time"
                    value={timeVal}
                    onChange={this.handleInputChange}
                    required
                    name="time"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="comment">Comment</label>
                  <textarea
                    className="form-control"
                    rows="3"
                    id="comment"
                    value={commentVal}
                    onChange={this.handleInputChange}
                    name="comment"
                  />
                </div>
                {usersSelect}
              </div>
              <div className="modal-footer">
                <button type="submit" className="btn btn-primary">
                  Save
                </button>
                <button type="button" className="btn btn-default" data-dismiss="modal">
                  Close
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

RepairModal.propTypes = {
  getUsers: PropTypes.func.isRequired,
  users: PropTypes.object.isRequired,
  usersHasErrored: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  users: state.rootReducer.users,
  usersHasErrored: state.rootReducer.usersHasErrored,
});

const mapDispatchToProps = dispatch => ({
  getUsers: () => dispatch(getAllUsers()),
});

export default connect(mapStateToProps, mapDispatchToProps)(RepairModal);
