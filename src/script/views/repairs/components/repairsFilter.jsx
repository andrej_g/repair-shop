import moment from 'moment';
import _ from 'lodash';
import { connect } from 'react-redux';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import DatePicker from 'react-datepicker';
import './repairsFilter.scss';
import { DATE_FORMAT } from '../../../globalConstants';
import { getAllUsers } from '../../users/actions';

class RepairsFilter extends Component {
  constructor(props) {
    super(props);

    this.state = {
      date: {},
      time: null,
      complete: true,
      incomplete: true,
      selectedUser: '',
    };

    this.handleDateChange = this.handleDateChange.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.toggleCheckbox = this.toggleCheckbox.bind(this);
    this.submitFilter = this.submitFilter.bind(this);
    this.resetFilter = this.resetFilter.bind(this);
    this.handleChangeSelect = this.handleChangeSelect.bind(this);
  }

  componentDidMount() {
    if (this.props.isMod) {
      this.props.getUsers();
    }
  }

  handleDateChange(e) {
    const that = this;
    this.setState({ date: e }, () => {
      that.submitFilter();
    });
  }

  handleInputChange(e) {
    const that = this;
    const nextState = {};
    nextState[e.target.name] = e.target.value;
    this.setState(nextState, () => {
      that.submitFilter();
    });
  }

  toggleCheckbox(event) {
    const that = this;
    if (event.target.id === 'filter-complete') {
      this.setState({ complete: !this.state.complete }, () => {
        that.submitFilter();
      });
    } else if (event.target.id === 'filter-incomplete') {
      this.setState({ incomplete: !this.state.incomplete }, () => {
        that.submitFilter();
      });
    }
    that.submitFilter();
  }

  handleChangeSelect(e) {
    const that = this;
    const userId = e.target.value;
    this.setState({ selectedUser: userId }, () => {
      that.submitFilter();
    });
  }

  submitFilter() {
    const filterObj = {};
    filterObj.date = moment(this.state.date, DATE_FORMAT, true).isValid()
      ? moment(this.state.date).format(DATE_FORMAT)
      : '';
    filterObj.time = ReactDOM.findDOMNode(document.getElementById('filter-time')).value;
    filterObj.complete = ReactDOM.findDOMNode(document.getElementById('filter-complete')).checked;
    filterObj.incomplete = ReactDOM.findDOMNode(
      document.getElementById('filter-incomplete'),
    ).checked;
    const pendingState = ReactDOM.findDOMNode(document.getElementById('filter-pending'));
    if (pendingState) {
      filterObj.pending = pendingState.checked;
    }
    if (this.props.isMod) {
      filterObj.userId = ReactDOM.findDOMNode(document.getElementById('filter-select-user')).value;
    }
    this.props.onFilterRepairs(filterObj);
  }

  resetFilter() {
    const that = this;
    if (this.props.isMod) {
      ReactDOM.findDOMNode(document.getElementById('filter-select-user')).selectedIndex = 0;
    }
    ReactDOM.findDOMNode(document.getElementById('filter-time')).value = '';
    this.setState(
      { date: {}, time: null, complete: true, incomplete: true, selectedUser: '' },
      () => {
        that.props.onResetFilter();
      },
    );
  }

  render() {
    let selectedDate = '';

    if (_.get(this.props.data, 'date')) {
      selectedDate = moment(this.props.data.date, DATE_FORMAT);
    }

    if (this.state.date && Object.keys(this.state.date).length > 0) {
      selectedDate = this.state.date;
    }

    let usersSelect = null;
    let pendings = null;

    if (this.props.isMod) {
      const usersInArray =
        this.props.users.userList && Object.keys(this.props.users.userList).length > 0
          ? this.props.users.userList.toArray()
          : [];

      const options = usersInArray.map(user => (
        <option key={user.id} value={user.id}>
          {user.email}
        </option>
      ));

      usersSelect = (
        <div className="form-group">
          <label htmlFor="usersSelect">Users</label>
          <select
            id="filter-select-user"
            name="usersSelect"
            className="form-control"
            onChange={this.handleChangeSelect.bind(this)}
          >
            <option value="">All users</option>
            {options}
          </select>
        </div>
      );

      pendings = (
        <div className="form-group">
          <div className="checkbox">
            <label htmlFor="filter-pending">
              <input type="checkbox" id="filter-pending" onChange={this.toggleCheckbox} />Show
              pending
            </label>
          </div>
        </div>
      );
    }

    return (
      <div className="filter-wrapper col-md-11">
        <div className="form-inline">
          <div className="wrapper-search form-group">
            <h3>Filter</h3>
            <div className="form-group">
              <label htmlFor="filter-date">Date (d/m/y)</label>
              <DatePicker
                onChange={this.handleDateChange}
                selected={selectedDate}
                id="filter-date"
                ref="filter-date"
                className="form-control"
                popoverAttachment="bottom center"
                popoverTargetAttachment="bottom center"
                popoverTargetOffset="0px 0px"
                showYearDropdown
                dateFormatCalendar="MMMM"
                locale="en-gb"
              />
            </div>
            <div className="form-group">
              <label htmlFor="filter-time">Time (hh:mm)</label>
              <input
                type="time"
                className="form-control"
                id="filter-time"
                onChange={this.handleInputChange}
                required
                name="filter-time"
              />
            </div>
            <div className="form-group">
              <div className="checkbox">
                <label htmlFor="filter-complete">
                  <input
                    type="checkbox"
                    id="filter-complete"
                    checked={this.state.complete}
                    onChange={this.toggleCheckbox}
                  />Complete
                </label>
              </div>
              <div className="checkbox">
                <label htmlFor="filter-incomplete">
                  <input
                    type="checkbox"
                    id="filter-incomplete"
                    checked={this.state.incomplete}
                    onChange={this.toggleCheckbox}
                  />Incomplete
                </label>
              </div>
            </div>
            {usersSelect}
            {pendings}
          </div>
          <button className="btn btn-default show-all" onClick={this.resetFilter}>
            <span>Reset</span>
          </button>
        </div>
      </div>
    );
  }
}

RepairsFilter.propTypes = {
  getUsers: PropTypes.func.isRequired,
  users: PropTypes.object.isRequired,
  usersHasErrored: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  users: state.rootReducer.users,
  usersHasErrored: state.rootReducer.usersHasErrored,
});

const mapDispatchToProps = dispatch => ({
  getUsers: () => dispatch(getAllUsers()),
});

export default connect(mapStateToProps, mapDispatchToProps)(RepairsFilter);
